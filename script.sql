CREATE DATABASE IF NOT EXISTS FINAL_PHP_DB;

USE FINAL_PHP_DB;

DROP TABLE IF EXISTS FINAL_PHP_DB.productos;
CREATE TABLE FINAL_PHP_DB.productos (
    id INT AUTO_INCREMENT PRIMARY KEY,
	codigo VARCHAR(10) UNIQUE DEFAULT NULL,
    descrip VARCHAR(255) NOT NULL,
    categoria VARCHAR(50) NOT NULL,
    marca VARCHAR(50) NOT NULL,
    stock_min INT DEFAULT 1,
    stock_max INT DEFAULT 10,
    stock_act INT DEFAULT 0 ,
    precio_venta DECIMAL(12, 2) DEFAULT 0.00
);

DROP TABLE IF EXISTS FINAL_PHP_DB.usuarios;
CREATE TABLE FINAL_PHP_DB.usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL
);

INSERT INTO FINAL_PHP_DB.usuarios(username, password) VALUES
('admin', 'admin'),
('Edwin', 'edwin'),
('Evelin', 'evelin'),
('Melany', 'melany');


-- SELECT * FROM usuarios WHERE username='edwin' AND password='edwin';
-- SELECT * FROM productos;

INSERT INTO FINAL_PHP_DB.productos (codigo, descrip, categoria, marca, stock_min, stock_max, precio_venta) VALUES
('P-0001', 'Camisa de manga larga XS para hombre', 'Casual', 'Charvet', 1, 90, 234.99),
('P-0002', 'Playera extragrande para hombre', 'Sport', 'Nike', 10, 100, 233.88),
('P-0003', 'Playera color rosa XS para dama', 'Sport', 'Charvet', 10, 50, 200),
('P-0004', 'Vestido rosa-bebe talla S', 'Vestidos', 'Veneta', 20, 180, 120.99),
('P-0005', 'Blazer para negocios para hombre', 'Trajes', 'Veneta', 10, 100, 133.90),
('P-0006', 'Falda columna de lana-barathea', 'Trajes', 'Veneta', 100, 1000, 120.99),
('P-0007', 'Vestido de raso con estampado', 'Vestidos', 'Veneta', 50, 500, 140.56),
('P-0008', 'Chamarra de vestir', 'Casual', 'Charvet', 10, 150, 299.99),
('P-0009', 'Vestido corto fruncido', 'Vestidos', 'Charvet', 5, 50, 289.98),
('P-0010', 'Camiseta de tirantes', 'Sport', 'Charvet', 10, 100, 178.00),
('P-0011', 'Pantalón recto de lana', 'Pantalones', 'Charvet', 20, 180, 129.99),
('P-0012', 'Chaleco de cachemir', 'Trajes', 'Charvet', 20, 200, 190.00),
('P-0013', 'Camiseta Lovelight', 'Trajes', 'Charvet', 10, 100, 100.99),
('P-0014', 'Pantalones Lovelight', 'Pantalones', 'Lorenzini', 10, 100, 100.00),
('P-0015', 'Camisa de algodón', 'Casual', 'Lorenzini', 20, 100, 89.99),
('P-0016', 'Jersey de cuello alto', 'Casual', 'Lorenzini', 10, 50, 99.00),
('P-0017', 'Jersey de cachemir', 'Casual', 'Osklen', 10, 40, 189.00),
('P-0018', 'Camisa de franela de lana', 'Trajes', 'Osklen', 20, 180, 123.99),
('P-0019', 'Jersey de lana de punto abierto', 'Casual', 'Lorenzini', 10, 100, 239.90),
('P-0020', 'Pantalones cropped de algodón', 'Pantalones', 'Lorenzini', 10, 100, 245.00),
('P-0021', 'Chaleco de lana a cuadros', 'Trajes', 'Niar', 10, 100, 140.99),
('P-0022', 'Sudadera de punto fino adornada', 'Sport', 'Niar', 5, 100, 123.00),
('P-0023', 'Pantalones de lana y lino', 'Pantalones', 'Niar', 5, 100, 100.00),
('P-0024', 'Chaqueta de punto fino', 'Casual', 'Chinawife', 20, 250, 180.00),
('P-0025', 'Camisa oversized estampada', 'Casual', 'Niar', 10, 100, 190.99),
('P-0026', 'Polo en piqué de algodón a rayas', 'Sport', 'Chinawife', 10, 50, 99.99),
('P-0027', 'Camiseta de punto de algodón', 'Sport', 'Chinawife', 10, 100, 99.90),
('P-0028', 'Blazer de lana y lino a cuadros', 'Trajes', 'Chinawife', 10, 100, 100.90),
('P-0029', 'Cárdigan de lana bordado', 'Casual', 'Chinawife', 10, 100, 167.90),
('P-0030', 'Camisa de algodón a rayas', 'Casual', 'Chinawife', 10, 100, 160.90),
('P-0031', 'Bermudas de sarga de algodón', 'Casual', 'Maroc', 10, 100, 178.90),
('P-0032', 'Chaleco de lana con Horsebit', 'Trajes', 'Maroc', 20, 180, 99.99),
('P-0033', 'Chaqueta de lona GG reversible', 'Trajes', 'Chinawife', 10, 100, 110.90),
('P-0034', 'Shorts de lona', 'Casual', 'Sport', 10, 100, 198.90),
('P-0035', 'Jersey de algodón y cachemir', 'Casual', 'Chinawife', 10, 100, 190.99),
('P-0036', 'Pantalones rectos de sarga', 'Patalones', 'Maroc', 10, 100, 100.99),
('P-0037', 'Bañador de jacquard GG', 'Sport', 'Maroc', 10, 100, 178.99),
('P-0038', 'Blazer de sarga de botonadura', 'Trajes', 'Chinawife', 10, 100, 234.90),
('P-0039', 'Chaleco de gabardina con botones', 'Trajes', 'Maroc', 20, 180, 345.99),
('P-0040', 'Pantalones ajustados con raya Web', 'Pantalones', 'Maroc', 10, 300, 349.00),
('P-0041', 'Chaqueta de lana de doble botonadura', 'Trajes', 'Frame', 10, 200, 168.00),
('P-0042', 'Pantalones rectos de sarga de algodón', 'Pantalones', 'Frame', 10, 230, 199.99),
('P-0043', 'Camisa de algodón a rayas GG', 'Casual', 'Maroc', 5, 200, 179.00),
('P-0044', 'Polo de mezcla de algodón bordado', 'Sport', 'Maroc', 10, 160, 349.00),
('P-0045', 'Sudadera de punto fino de algodón', 'Sport', 'Maroc', 10, 120, 189.00),
('P-0046', 'Pantalones tapered de lana', 'Pantalones', 'Frame', 20, 180, 155.99),
('P-0047', 'Chinos de sarga de algodón', 'Casual', 'Frame', 10, 150, 144.00),
('P-0048', 'Camiseta de punto fino de algodón', 'Casual', 'Chinawife', 10, 100, 167.00),
('P-0049', 'Chaqueta bomber GG de ante', 'Casual', 'Frame', 10, 50, 189.00),
('P-0050', 'Chaqueta con brocado floral', 'Casual', 'Chinawife', 10, 50, 167.00),
('P-0051', 'Chaleco de lana y mohair', 'Trajes', 'Chinawife', 10, 50, 199.00),
('P-0052', 'Chaqueta en lona de nylon', 'Trajes', 'Frame', 10, 50, 134.00),
('P-0053', 'Pantalones de lana a cuadros', 'Pantalones', 'Chinawife', 10, 50, 122.00),
('P-0054', 'Cárdigan en punto de lana', 'Casual', 'Chinawife', 10, 40, 100.00),
('P-0055', 'Jeans tapered de tiro medio', 'Casual', 'Frame', 10, 80, 156.00);