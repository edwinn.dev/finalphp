<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <title>Enviar documentos via Gmail</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="../app/img/logo.svg" rel="icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link href="../app/css/bootstrap.min.css" rel="stylesheet">
    <link href="../app/css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container row align-center mt-5">
    <div class="col-4"></div>
    <div class="col-4">


<?php
  session_start();
  //Importar las clases de la libreria PHPmailer desde el directorio mail/src
  require '../mail/src/Exception.php';
  require '../mail/src/PHPMailer.php';
  require '../mail/src/SMTP.php';
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  //Obtener los datos del formulario
  $emailOrigen = $_POST["origen"];
  $password = $_POST["password"];
  $asunto = $_POST["asunto"];
  $mensaje = $_POST["mensaje"];
  $emailsDestino = $_POST["destino"];


  //Persistir el email origen en una sesion y recuperarlo cuando sea necesario
  $_SESSION['email_origen'] = $emailOrigen;
  $_SESSION['password_email'] = $password;


  //Llamar a la funcion que inicia el envio correo
  iniciarEnvio($emailOrigen, $password, $asunto, $mensaje, $emailsDestino);


  function iniciarEnvio($emailOrigen, $password, $asunto, $mensaje, $emailsDestino){
    
    //Obtener la ruta del archivo de envio
    $carpetaDestino="../download/";
    if(!file_exists($carpetaDestino)) {
      @mkdir($carpetaDestino);
    }

    if(isset($_FILES["archivo"]) && $_FILES["archivo"]["name"][0]) {
      for($i=0;$i<count($_FILES["archivo"]["name"]);$i++) {
        
        
        //Validar si el archivo es algun tipo de imagen
        if($_FILES["archivo"]["type"][$i]=="image/png" || $_FILES["archivo"]["type"][$i]=="image/jpeg" || $_FILES["archivo"]["type"][$i]=="image/pjpeg" || $_FILES["archivo"]["type"][$i]=="image/gif") {
          $origen=$_FILES["archivo"]["tmp_name"][$i];
          $destino=$carpetaDestino.$_FILES["archivo"]["name"][$i];
          if(@move_uploaded_file($origen, $destino)) {
              $imagen = "../download/".$_FILES["archivo"]["name"][$i];
              $res = enviarCorreo($emailOrigen, $password, $asunto, $mensaje, $emailsDestino, $imagen);
              mostrarAlerta($res);
          } else {
              echo "<br>No se ha podido guardar el archivo: ".$_FILES["archivo"]["name"][$i];
          }
        } 


        //Validar si el archivo en un PDF
        if($_FILES["archivo"]["type"][$i]=="application/pdf") {
          $origen=$_FILES["archivo"]["tmp_name"][$i];
          $destino=$carpetaDestino.$_FILES["archivo"]["name"][$i];
          if(@move_uploaded_file($origen, $destino)) {
            $pdf = $carpetaDestino.$_FILES["archivo"]["name"][$i];
            $res = enviarCorreo($emailOrigen, $password, $asunto, $mensaje, $emailsDestino, $pdf);
            mostrarAlerta($res);
          } else {
            echo "<br>No se ha podido guardar el archivo: ".$_FILES["archivo"]["name"][$i];
          }
        }


        //Validar si el archivo es un EXCEL
        if($_FILES["archivo"]["type"][$i]=="application/vnd.ms-excel" || $_FILES["archivo"]["type"][$i]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
          $origen=$_FILES["archivo"]["tmp_name"][$i];
          $destino=$carpetaDestino.$_FILES["archivo"]["name"][$i];
          if(@move_uploaded_file($origen, $destino)) {
            $excel = $carpetaDestino.$_FILES["archivo"]["name"][$i];
            $res = enviarCorreo($emailOrigen, $password, $asunto, $mensaje, $emailsDestino, $excel);
            mostrarAlerta($res);
          } else {
            echo "<br>No se ha podido guardar el archivo: ".$_FILES["archivo"]["name"][$i];
          }
        }
      }
    } else {
      echo "<br>No hay archivos adjuntos<br>";
      $res = enviarCorreo($emailOrigen, $password, $asunto, $mensaje, $emailsDestino, null);
      mostrarAlerta($res);
    }
  }
    

  //Funcion para enviar correo
  function enviarCorreo($origen, $password, $subject, $mensaje, $destino, $archivo){
    $mail = new PHPMailer(true);
    try {
      //Configuracion del servidor de corrreo
      $mail->isSMTP();
      $mail->Host       = 'smtp.gmail.com';
      $mail->SMTPAuth   = true;
      $mail->Username   = $origen;
      $mail->Password   = $password;
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
      $mail->Port       = 465;

      $mail->setFrom($origen);

      //Receptores o correos destino
      $emails = explode(",", str_replace(" ", "", $destino));
      for ($index=0; $index < count($emails); $index++) {
        //Recuperar cada email que se separa por coma (,)
        $mail->addAddress($emails[$index]);
      }

      if($archivo != NULL){
        $rutaArchivo = explode("/", $archivo);
        //Indicar que se enviara un archivo: Imagen, PDF o EXCEL
        $mail->addAttachment($archivo, utf8_decode($rutaArchivo[count($rutaArchivo) - 1]) );
      }

      //Contenido del mensaje, tambien se puede enviar plantillas HTML
      $mail->isHTML(true);                                 
      $mail->Subject = utf8_decode($subject);
      $mail->Body    = utf8_decode($mensaje);

      //Verificar si el correo se envio correctamente
      if($mail->send()){
        return true;
      }
      return false;
    } catch (Exception $e) {
      echo "Error al enviar correo {$mail->ErrorInfo}";
      return false;
    }
  }


  //Funcion para mostrar el mensaje de envio
  function mostrarAlerta($arg){
    if($arg){
      echo '<div class="alert alert-success" role="alert">
      Mensaje enviado
      <hr>
        <a href="compartir.php" class="btn btn-primary">Regresar</a>
      </div>';
    } else {
      echo '<div class="alert alert-danger" role="alert">
      Error al enviar correo, verifique el correo de origen y los correos destino deben estar separados por coma (,). Ademas debe contar con conexion a internet
      <hr>
        <a href="compartir.php" class="btn btn-primary">Regresar</a>
      </div>';
    }
  }
?>


  </div>
  <div class="col-4"></div>
</div>
</body>
</html>
