<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <title>Enviar documentos via WhatsApp</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="../app/img/logo.svg" rel="icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link href="../app/css/bootstrap.min.css" rel="stylesheet">
    <link href="../app/css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container row align-center mt-5">
    <div class="col-4"></div>
    <div class="col-4">
    
<?php
  session_start();
  //Importar la libreria UltraMSG desde la carpeta: wsdk
  require_once('../wsdk/ultramsg.class.php');
  
  
  //Obtener los datos del formulario
  $para = $_POST["destino"];
  $mensaje = $_POST["mensaje"];
  $token= $_POST["token"];
  $instance_id= $_POST["id"];
  $cliente = new UltraMsg\WhatsAppApi($token, $instance_id);


  //Persistir el ID de instancia y el token en una sesion y recuperarlo cuando sea necesario
  $_SESSION['id'] = $instance_id;
  $_SESSION['token'] = $token;


  //Llamar a la funcion que inica el envio del mensaje
  iniciarEnvio($para, $mensaje, $cliente);

  
  function iniciarEnvio($para, $mensaje, $cliente){
    //Obtener la ruta del archivo de envio
    $carpetaDestino="../download/";
    if(!file_exists($carpetaDestino)) {
      @mkdir($carpetaDestino);
    }

    if(isset($_FILES["archivo"]) && $_FILES["archivo"]["name"][0]) {
      for($i=0;$i<count($_FILES["archivo"]["name"]);$i++) {

        //Validar si el archivo es algun tipo de imagen
        if($_FILES["archivo"]["type"][$i]=="image/png" || $_FILES["archivo"]["type"][$i]=="image/jpeg" || $_FILES["archivo"]["type"][$i]=="image/pjpeg" || $_FILES["archivo"]["type"][$i]=="image/gif") {
          $origen=$_FILES["archivo"]["tmp_name"][$i];
          $destino=$carpetaDestino.$_FILES["archivo"]["name"][$i];
          if(@move_uploaded_file($origen, $destino)) {
              $imagen = $carpetaDestino.$_FILES["archivo"]["name"][$i];
              $api = enviarImagen($para, $mensaje, $imagen, $cliente);
              mostrarAlerta($api);
          } else {
              echo "<br>No se ha podido guardar el archivo: ".$_FILES["archivo"]["name"][$i];
          }
        } 


        //Validar si el archivo en un PDF
        if($_FILES["archivo"]["type"][$i]=="application/pdf") {
          $origen=$_FILES["archivo"]["tmp_name"][$i];
          $destino=$carpetaDestino.$_FILES["archivo"]["name"][$i];
          if(@move_uploaded_file($origen, $destino)) {
            $pdf = $carpetaDestino.$_FILES["archivo"]["name"][$i];
            $api = enviarDocumento($para, $mensaje, $pdf, $cliente);
            mostrarAlerta($api);
          } else {
            echo "<br>No se ha podido guardar el archivo: ".$_FILES["archivo"]["name"][$i];
          }
        }


        //Validar si el archivo es un EXCEL
        if($_FILES["archivo"]["type"][$i]=="application/vnd.ms-excel" || $_FILES["archivo"]["type"][$i]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
          $origen=$_FILES["archivo"]["tmp_name"][$i];
          $destino=$carpetaDestino.$_FILES["archivo"]["name"][$i];
          if(@move_uploaded_file($origen, $destino)) {
            $excel = $carpetaDestino.$_FILES["archivo"]["name"][$i];
            $api = enviarDocumento($para, $mensaje, $excel, $cliente);
            mostrarAlerta($api);
          } else {
            echo "<br>No se ha podido guardar el archivo: ".$_FILES["archivo"]["name"][$i];
          }
        }
      }
    } else {
      echo "<br>No hay archivos adjuntos<br>";
      $api = enviarMensaje($para, $mensaje, $cliente);
      mostrarAlerta($api);
    }
  }



  //Funcion para enviar solo mensaje, sin ningun archivo adjunto
  function enviarMensaje($para, $contenido, $cliente){
    //Separar cada telefono por coma (,)
    $destino = explode(",", str_replace(" ", "", $para));
    for ($index=0; $index < count($destino); $index++) { 
      $api = $cliente->sendChatMessage($destino[$index] ,$contenido);
    }
    return $api;
  }



  //Function para enviar una imagen de cualquier tipo
  function enviarImagen($para, $descrip, $imagen, $cliente) {
    $documento = base64_encode(file_get_contents($imagen));
    //Separar cada telefono por coma (,)
    $destino = explode(",", str_replace(" ", "", $para));
    for ($index=0; $index < count($destino); $index++) { 
      $api = $cliente->sendImageMessage($destino[$index] ,$descrip, $documento);
    }
    return $api;
  }
  


  //Funcion para enviar un documento: PDF o EXCEL
  function enviarDocumento($para, $mensaje, $doc, $cliente) {
    $documento = base64_encode(file_get_contents($doc));
    $nombreArchivo = explode("/", $doc); //Obtener el nombre del archivo

    //Separar cada telefono por coma (,)
    $destino = explode(",", str_replace(" ", "", $para));
    for ($index=0; $index < count($destino); $index++) {
      $cliente->sendChatMessage($destino[$index], $mensaje, $cliente);
      $api = $cliente->sendDocumentMessage($destino[$index], $nombreArchivo[count($nombreArchivo) - 1], $documento);
    }
    return $api;
  }

  

  //Funcion para mostrar el mensaje de envio
  function mostrarAlerta($api){
    foreach($api as $i => $val){
      if($val == "ok"){
        echo '<div class="alert alert-success" role="alert">
        Mensaje enviado
        <hr>
          <a href="compartir.php" class="btn btn-primary">Regresar</a>
        </div>';
        return;
      }
    }

    echo '<div class="alert alert-danger" role="alert">
    Error al enviar mensaje de WhatsApp, Verifique el telefono origen y los telefonos destino deben estar separados por coma (,). Ademas debe contar con conexion a internet
    <hr>
      <a href="compartir.php" class="btn btn-primary">Regresar</a>
    </div>';
  }

?>


    </div>
    <div class="col-4"></div>
  </div>
</body>
</html>



