<?php
  
  function conectar() {
    $host = "localhost"; 
    $user = "root"; 
    $pass = "admin";
    $db = "FINAL_PHP_DB";

    $conexion = mysqli_connect($host, $user, $pass, $db);

    if(!$conexion){
      die("Error al conectar con la base de datos : " . mysqli_connect_error());
    }
    
    return $conexion;
  }
?>