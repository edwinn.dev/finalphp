<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <title>Exportar a PDF</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="../app/img/logo.svg" rel="icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link href="../app/css/bootstrap.min.css" rel="stylesheet">
    <link href="../app/css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container row align-center mt-5">
    <div class="col-2"></div>
    <div class="col-8">



<?php
  require("../pdf/fpdf.php");
  require('conexion.php');

  class PDF extends FPDF {

    function crearTabla($cabecera, $datos) {
      $this->SetFillColor(0, 120, 170);
      $this->SetTextColor(255);
      $this->SetDrawColor(0, 120, 170);
      $this->SetLineWidth(0.2);
      $this->SetFont('Arial','B', 8);
      
      $w = array(8, 15, 65, 20, 20, 15, 15, 15, 15);
      for($i=0;$i<count($cabecera);$i++)
        $this->Cell($w[$i], 7, utf8_decode($cabecera[$i]), 1, 0, 'C', true);
      $this->Ln();
  
      $this->SetFillColor(224,235,255);
      $this->SetTextColor(0);
      $this->SetFont('Arial','', 9);
  
      $fill = false;
      foreach($datos as $index => $producto){
        $this->Cell($w[0],6,($index + 1),'LR',0,'C',$fill);
        $this->Cell($w[1],6,utf8_decode($producto["codigo"]),'LR',0,'L',$fill);
        $this->Cell($w[2],6,utf8_decode($producto["descrip"]),'LR',0,'L',$fill);
        $this->Cell($w[3],6,utf8_decode($producto["categoria"]),'LR',0,'L',$fill);
        $this->Cell($w[4],6,utf8_decode($producto["marca"]),'LR',0,'L',$fill);
        $this->Cell($w[5],6,number_format($producto["stock_min"], 2),'LR',0,'R',$fill);
        $this->Cell($w[6],6,number_format($producto["stock_max"], 2),'LR',0,'R',$fill);
        $this->Cell($w[7],6,number_format($producto["stock_act"], 2),'LR',0,'R',$fill);
        $this->Cell($w[8],6,number_format($producto["precio_venta"], 2),'LR',0,'R',$fill);
        $this->Ln();
        $fill = !$fill;
      }
      $this->Cell(array_sum($w),0,'','T');
    }

    function Header(){
      $this->Image('../app/img/logo.png', 5, 5, 30);
      $this->SetFont('Arial', 'B', 16);
      $this->Cell(80);
      $this->Cell(50, 10, utf8_decode("Store Fashion"), 0, 0, 'C');
      $this->Ln();
      $this->Cell(30);
      $this->SetFont('Arial', '', 8);
      $this->Cell(50, 10, utf8_decode("Direccion: Comas, Lima - Peru"), 0, 0, 'L');
      $this->Cell(50, 10, utf8_decode("Telefono: +51 923 928 727"), 0, 0, 'L');
      $this->Cell(50, 10, utf8_decode("Email: ccf@phpgroup.com"), 0, 0, 'L');
      $this->SetDrawColor(0, 120, 170);
      $this->SetLineWidth(0.4);
      $this->Line(40, $this->GetY()+10, $this->GetX()+8, $this->GetY()+10);
      $this->Ln(20);
    }
  
    function Footer() {
      $this->SetY(-15);
      $this->SetFont('Arial','I', 10);
      $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
      $this->SetY(-13);
      $this->SetX(-25);
      $this->Image('../app/img/store.png', null, null, 15);
    }
  }

  $fecha = date('d-m-Y');
  $pdf = new PDF();
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont('Arial', 'B', 11);
  $pdf->Cell(20, 8, "Asunto : ", 0, 0, 'L');
  $pdf->SetFont('Arial', '', 11);
  $pdf->Cell(100, 8, "Reportes de productos", 0, 0, 'L');
  $pdf->Ln();
  $pdf->SetFont('Arial', 'B', 11);
  $pdf->Cell(20, 8, "Fecha  : ", 0, 0, 'L');
  $pdf->SetFont('Arial', '', 11);
  $pdf->Cell(100, 8, $fecha, 0, 0, 'L');
  $pdf->Ln(15);
  $cabecera = array("#", "CODIGO", "DESCRIPCION", "CATEGORIA", "MARCA", "S. MIN", "S. MAX", "S. ACT", "P.V ($)");
  $datos = obtenerProductos();
  $pdf->crearTabla($cabecera, $datos);

  $rutaAlmac = obtener_ruta();
  $rutaArchivo = $rutaAlmac."Reporte productos - $fecha.pdf";
  $pdf->Output("F", $rutaArchivo);

  $fileName = explode("/", $rutaArchivo);

  //Mostrar el mensaje del estado de la exportacion
  echo '<div class="alert alert-success" role="alert">
      Exportacion exitosa <hr>Disponible en: C:/xampp/htdocs/finalphp/download/'.$fileName[count($fileName) - 1].'
      <hr>
        <a href="productos.php">Regresar a productos</a>
      </div>';

  function obtenerProductos() {
    $conn = conectar();
    $consulta = "SELECT * FROM productos";
    $productos = mysqli_query($conn, $consulta);
    return $productos;
  }

  function obtener_ruta() {
    $carpetaDestino="../download/";
    if(!file_exists($carpetaDestino)) {
      if(@mkdir($carpetaDestino)) return $carpetaDestino;
      else return "./";
    }
    return $carpetaDestino;
  }
?>



</div>
  <div class="col-2"></div>
</div>
</body>
</html>
