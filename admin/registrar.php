<?php
  require('conexion.php');

  $categoria = $_POST['categoria'];
  $marca = $_POST['marca'];
  $codigo = $_POST['codigo'];
  $descrip = $_POST['descrip'];
  $stockMin = $_POST['stockMin'];
  $stockMax = $_POST['stockMax'];
  $pVenta = $_POST['precioVenta'];

  $conn = conectar();

  if(isset($_GET["update"])){
    $id = $_POST["id"];
    $consulta = "UPDATE productos SET codigo='$codigo', descrip='$descrip', categoria='$categoria', marca='$marca', stock_min=$stockMin, stock_max=$stockMax, precio_venta=$pVenta WHERE id=$id";
    if (mysqli_query($conn, $consulta)) {
      //Si el producto se actualizo en la BD, regresa a la pagina productos.php
      Header('Location: productos.php?update=true');
    } else {
      echo "error :::: " . mysqli_error($conn);
    }
  } else {
    $consulta = "INSERT INTO productos (codigo, descrip, categoria, marca, stock_min, stock_max, precio_venta) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($consulta);
    $stmt->bind_param('ssssiid', $codigo, $descrip, $categoria, $marca, $stockMin, $stockMax, $pVenta);
    if ($stmt->execute()) {
      //Si el producto se agrego en la BD, regresa a la pagina productos.php
      Header('Location: productos.php?new=true');
    } 
  }
  
?>