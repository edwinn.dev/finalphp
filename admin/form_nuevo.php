<?php
  session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Nuevo producto</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <link href="../app/img/logo.svg" rel="icon">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
  <link href="../app/css/bootstrap.min.css" rel="stylesheet">
  <link href="../app/css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container-fluid position-relative d-flex p-0">
    <div id="spinner" class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
      <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>

    <div class="sidebar pe-4 pb-3">
      <nav class="navbar bg-secondary navbar-dark">
        <a class="navbar-brand mx-4 mb-3">
          <h6 class="text-primary">Panel de Control</h6>
        </a>
        <div class="d-flex align-items-center ms-4 mb-4">
          <div class="position-relative">
            <img class="rounded-circle" src="../app/img/user.png" alt="" style="width: 40px; height: 40px;">
            <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
          </div>
          <div class="ms-3">
            <h6 class="mb-0"><?php echo $_SESSION['username'] ?></h6>
            <span>Admin</span>
          </div>
        </div>
        <div class="navbar-nav w-100">
          <div class="navbar-nav w-100">
            <a href="index.php" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
            <a href="productos.php" class="nav-item nav-link active"><i class="fa fa-th me-2"></i>Productos</a>
            <a href="configuracion.php" class="nav-item nav-link"><i class="fa fa-share-alt me-2"></i>Compartir</a>
          </div>
        </div>
      </nav>
    </div>

    <div class="content">
      <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
        <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
          <h2 class="text-primary mb-0"><i class="fa fa-user-edit"></i></h2>
        </a>
        <a href="#" class="sidebar-toggler flex-shrink-0">
          <i class="fa fa-bars"></i>
        </a>
        <div class="navbar-nav align-items-center ms-auto">
          <div class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
              <img class="rounded-circle me-lg-2" src="../app/img/user.png" alt="" style="width: 40px; height: 40px;">
              <span class="d-none d-lg-inline-flex"><?php echo $_SESSION['username'] ?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
              <a href="../index.html" class="dropdown-item">Salir</a>
            </div>
          </div>
        </div>
      </nav>

      <div class="container-fluid pt-4 px-4">
        <div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="bg-secondary rounded align-items-center justify-content-between p-4">
              <div class="ms-3">
                
                <form action="registrar.php" method="POST">
                  <div class="mb-3">
                    <label class="form-label">CODIGO</label>
                    <input type="text" class="form-control" name="codigo">
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Descripcion o nombre del producto</label>
                    <textarea class="form-control" rows="2" name="descrip" required></textarea>
                  </div>
                  <div class="row">
                    <div class="col-6">
                      <div class="mb-3">
                        <label class="form-label">Stock Minimo</label>
                        <input type="number" class="form-control" value="1" name="stockMin">
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="mb-3">
                        <label class="form-label">Stock Maximo</label>
                        <input type="number" class="form-control" value="50" name="stockMax">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-6">
                      <div class="mb-3">
                        <label class="form-label">Categoria</label>
                        <input type="text" class="form-control" name="categoria" value="Sin categoria" required>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="mb-3">
                        <label class="form-label">Marca</label>
                        <input type="text" class="form-control" name="marca" value="Sin marca" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-6">
                      <div class="mb-3">
                        <label class="form-label">Precio venta ($)</label>
                        <input type="text" class="form-control" value="0.0" name="precioVenta">
                      </div>
                    </div>

                    <div class="col-6">
                      <div class="mb-3">
                        <br>
                        <input class="btn btn-sm btn-success" type="submit" value="Guardar producto">
                      </div>
                    </div>
                  </div>
                </form>


              </div>
            </div>
          </div>
          <div class="col-3"></div>
        </div>
      </div>
    </div>

    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
  </div>

  <!-- Librerias JavaScript -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../app/js/main.js"></script>
</body>
</html>