<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Productos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="../app/img/logo.svg" rel="icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link href="../app/css/bootstrap.min.css" rel="stylesheet">
    <link href="../app/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid position-relative d-flex p-0">
        <div id="spinner" class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>


        <div class="sidebar pe-4 pb-3">
            <nav class="navbar bg-secondary navbar-dark">
                <a class="navbar-brand mx-4 mb-3">
                    <h6 class="text-primary">Panel de Control</h6>
                </a>
                <div class="d-flex align-items-center ms-4 mb-4">
                    <div class="position-relative">
                        <img class="rounded-circle" src="../app/img/user.png" alt="" style="width: 40px; height: 40px;">
                        <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                    </div>
                    <div class="ms-3">
                        <h6 class="mb-0"><?php echo $_SESSION['username'] ?></h6>
                        <span>Admin</span>
                    </div>
                </div>
                <div class="navbar-nav w-100">
                    <a href="index.php" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
                    <a href="productos.php" class="nav-item nav-link active"><i class="fa fa-th me-2"></i>Productos</a>
                    <a href="compartir.php" class="nav-item nav-link"><i class="fa fa-share-alt me-2"></i>Compartir</a>
                </div>
            </nav>
        </div>


        <div class="content">
            <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
                <a class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars cursor-pointer"></i>
                </a>
                <?php
                    if(isset($_GET["new"])){
                        echo '<div class="alert alert-success mt-3" role="alert">Se ha creado un nuevo producto</div>';
                    } else if(isset($_GET["update"])){
                        echo '<div class="alert alert-info mt-3" role="alert">Producto actualizado</div>';
                    } else if(isset($_GET["delete"])){
                        echo '<div class="alert alert-danger mt-3" role="alert">Producto eliminado</div>';
                    }
                ?>

                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <img class="rounded-circle me-lg-2" src="../app/img/user.png" alt="" style="width: 40px; height: 40px;">
                            <span class="d-none d-lg-inline-flex"><?php echo $_SESSION['username'] ?></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
                            <a href="../index.php" class="dropdown-item">Salir</a>
                        </div>
                    </div>
                </div>
            </nav>


            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Productos</h6>
                        <a href="form_nuevo.php" class="btn btn-success">Nuevo producto <i class="fa fa-plus" aria-hidden="true"></i></a>
                        <form action="productos.php?buscar=true" method="POST" class=" d-md-flex ms-4">
                            <input name="buscar" class="form-control bg-dark border-0" type="search" placeholder="Nombre del producto" required>
                            <input type="submit" class="btn btn-info" value="Buscar">
                        </form>
                        <a href="productos.php" class="btn btn-info">Ver todo</a>
                        <a href="exportar.php">Exportar a PDF</a>
                    </div>
                    <hr>
                    <div class="table-responsive">


                        <table class="table text-start align-middle table-bordered table-hover mb-0">
                            <thead>
                                <tr class="text-white">
                                    <th scope="col">#</th>
                                    <th scope="col">Codigo</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Categoria</th>
                                    <th scope="col">Marca</th>
                                    <th scope="col">S. Actual</th>
                                    <th scope="col">P.Ven ($)</th>
                                    <th scope="col">Accion</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                require("conexion.php");
                                $conn = conectar();

                                $consulta = "SELECT * FROM productos";
                                
                                if(isset($_GET["buscar"])){
                                    $nombreProducto = $_POST["buscar"];
                                    $consulta = "SELECT * FROM productos WHERE descrip LIKE '%$nombreProducto%'";
                                }

                                $productos = mysqli_query($conn, $consulta);
                                foreach ($productos as $index => $producto) {
                                ?>

                                    <tr>
                                        <td> <?php echo $index + 1 ?></td>
                                        <td> <?php echo $producto["codigo"] ?></td>
                                        <td> <?php echo $producto["descrip"] ?> </td>
                                        <td> <?php echo $producto["categoria"] ?></td>
                                        <td> <?php echo $producto["marca"] ?></td>
                                        <td> <?php echo $producto["stock_act"] ?></td>
                                        <td> <?php echo $producto["precio_venta"] ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-success" href=<?php echo "form_actualizar.php?id=".$producto["id"]?> ><i class="fa fa-check-square"></i></a>
                                            <a class="btn btn-sm btn-danger" href=<?php echo "eliminar_producto.php?id=".$producto["id"]?> ><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>

                                <?php
                                }
                                ?>


                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

        </div>

        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- Librerias JavaScript -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../app/js/main.js"></script>
</body>

</html>